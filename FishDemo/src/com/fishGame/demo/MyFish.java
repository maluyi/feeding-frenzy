package com.fishGame.demo;

import javax.swing.*;
import java.awt.*;

public class MyFish extends Fish{
   int x1,y1;

    public MyFish(){
        length = 68; height=55;
        //界面坐标 可以随机出现在某个地图里
        x = 100;
        y = 450;
        x1=x+68;
        y1=y+55;
        img = new ImageIcon("images/fish08.png").getImage();
  }

    /**
     * 绘制的功能
     * @param g
     */
    @Override
    public void draw(Graphics g) {

        if (isMoveLeft) {
            sx1 = 0;
            sy1 = 0;
            sx2 = length;
            sy2 = height;
        } else {
            sx1 = length;
            sy1 = height;
            sx2 = 0;
            sy2 = 0;
        }
        //画图 优化上下左右个图片
        g.drawImage(img, x, y, x1, y1, sx1, sy1, sx2, sy2, null);
    }
        public void moveLeft(){
           isMoveLeft=true;
            if(x>=10) {
                x-=10;
                x1-=10;
            }
        }
        public void moveRight(){
            isMoveLeft=false;
            if(x<=1350) {
                x+=10;
                x1+=10;}
        }
        public void moveUp(){
            if(y>=10) {
                y-=10;
                y1-=10;
            }
        }
        public void moveDown(){
            if(y<=800) {
                y+=10;
                y1+=10;
            }
        }

}
