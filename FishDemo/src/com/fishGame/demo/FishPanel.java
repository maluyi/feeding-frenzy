package com.fishGame.demo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class FishPanel extends JPanel {
    //0:游戏加载 1：游戏开始 2：游戏赞同 3：游戏结束
    int state = 0;
    MyFish myFish = new MyFish();
    List<Fish>fishList = new ArrayList<>();//装其他鱼的集合
    /**
     *
     * @param g 画笔对象
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //绘制游戏的背景界面
        switch (state){
            case 0:Image image =new ImageIcon("images/start.png").getImage();
                g.drawImage(image,0,0,1440,900,null);
                break;
            case 1://背景绘制
                drawBg(g);
                //绘制己方的小鱼
                drawFish(g);
                //绘制敌方小鱼
                drawFishes(g);
                break;
            case 2:
                break;
            case 3:
                break;

        }

    }

    private void drawFishes(Graphics g) {
        for (int i = 0; i < fishList.size(); i++) {
            Fish fish = fishList.get(i);
            fish.draw(g);
        }
    }

    /**
     * 绘制己方小鱼的方法
     * @param g
     */
    private void drawFish(Graphics g) {
        myFish.draw(g);
    }

    /**
     * 绘制游戏开始的背景
     * @param g
     */
     private void drawBg(Graphics g) {
         Image image=new ImageIcon("images/bg.jpg").getImage();
         g.drawImage(image,0,0,1440,900,null);

    }

    /**
     * 键盘监听事件执行的方法
     */
    public void move(int keycode){
        switch (keycode){
            case KeyEvent.VK_SPACE:
                if(state==0){
                    state = 1;
                }
            //空格键，进入游戏开始的界面
                break;
            case KeyEvent.VK_LEFT:
                myFish.moveLeft();
                break;
            case KeyEvent.VK_RIGHT:
                myFish.moveRight();
                break;
            case KeyEvent.VK_UP:
                myFish.moveUp();
                break;
            case KeyEvent.VK_DOWN:
                myFish.moveDown();
                break;
        }
        repaint();
    }
}
