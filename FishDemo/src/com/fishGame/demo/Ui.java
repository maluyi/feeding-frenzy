package com.fishGame.demo;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * 步骤：
 * 1.创建窗体对象
 * 2.背景的绘制
 * 3.键盘的监听事件
 *      1.实现接口
 *      2.添加监听事件
*  4.将己方的鱼方上去
 * 5.让小鱼动起来
 * 6.敌方的鱼动起来 先创建集合装起来再获取对象
 */
public class Ui extends JFrame implements KeyListener {
    private FishPanel fishPanel = new FishPanel();
    public Ui(){
        this.setTitle("大鱼吃小鱼");
        this.setSize(1440,900);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.add(fishPanel);
        this.setVisible(true);
        this.addKeyListener(this);
    }

    public static void main(String[] args) {
        new Ui();
    }

    @Override
    public void keyPressed(KeyEvent e) {
       //监听按键 根据不同的按键执行不同的操作
        fishPanel.move(e.getKeyCode());

    }







    @Override
    public void keyReleased(KeyEvent e) {

    }
    @Override
    public void keyTyped(KeyEvent e) {

    }
}
