package ahau.finalProject.fish;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class Music {
  
	//属性
	URL bgm_url,eat_url,level_url,death_url;
	AudioClip eat,bgm,levelup,death;
	public Music() {
		try {
			bgm_url = new File("musci/bgm.wav").toURL();
			eat_url = new File("musci/eat.wav").toURL();
			level_url = new File("musci/levelup.wav").toURL();
			death_url = new File("musci/gameover.wav").toURL();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		eat = Applet.newAudioClip(eat_url);
		bgm = Applet.newAudioClip(bgm_url);
		levelup = Applet.newAudioClip(level_url);
		death = Applet.newAudioClip(death_url);
	}
}