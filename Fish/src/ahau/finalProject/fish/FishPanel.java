package ahau.finalProject.fish;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JPanel;



public class FishPanel extends JPanel{
	//标识状态0开始，1运行，2暂停，3是结束
		public int state=START;
		public static final int START=0;
		public static final int RUNNING=1;
		public static final int PAUSE=2;
		public static final int GAMEOVER=3;
	    Image bkImg = new ImageIcon("images/bg.jpg").getImage();
	    Image over = new ImageIcon("images/bg.jpg").getImage();
	    Image zan = new ImageIcon("images/bg.jpg").getImage();
	    Image start = new ImageIcon("images/start.png").getImage();
	    myFish fish;
	    Music music;
	   int score=0;
	   int oldzhanli;
	  ArrayList<Fish> fishs = new ArrayList<Fish>();
	  int mini=0,mid=1,big=1,superfish=1;
	
	public FishPanel() {
		fish = new myFish();
	     music = new Music();
	     oldzhanli=fish.zhanli;
	}
	
	public void paint(Graphics g)  {
				super.paint(g);
				System.out.println("aaa");
				switch(state) {
				case START:
					g.drawImage(start, 0, 0, 1440, 900, 0, 0, 500,300, null);break;
				case RUNNING:
					drawBackGround(g);
					drawMyFish(g);
					drawFishs(g);
					g.setColor(Color.white);
					Font f = new Font("黑体",Font.BOLD,20);
					g.setFont(f);  
					g.drawString("当前得分:"+score, 15, 20);
					g.drawString("战斗力:"+fish.zhanli, 200, 20);break;
				case PAUSE:
					g.drawImage(zan, 0, 0, null);break;
				case GAMEOVER:
					//g.drawImage(over, 0, 0, null);break;
					drawBackGround(g);
					drawMyFish(g);
					drawFishs(g);
					g.drawString("当前得分:"+score, 15, 20);
					g.setColor(Color.white);
					Font ff = new Font("黑体",Font.BOLD,50);
					g.setFont(ff);
					g.drawString("GAME　OVER",600 ,450);
			}
 	}
	
	 private void createFish() {
		  if((mini++%400)==0) {
			  fishs.add(new miniFish());
			  }
			if((mid++%300)==0) {
				fishs.add(new middleFish());
				}
			if((big++%1000)==0) {
				fishs.add(new bigFish());
		 }
			if((superfish++%5000)==0) {
				fishs.add(new superFish());
		 }
	  }

	private void drawFishs(Graphics g) {
		// TODO Auto-generated method stub
		for(int i=0;i<fishs.size();i++) {
			fishs.get(i).draw(g);
		}
	}
	
	private void drawMyFish(Graphics g) {
		// TODO Auto-generated method stub
		fish.draw(g);
	}
	
	private void drawBackGround(Graphics g) {
		// TODO Auto-generated method stub
		g.drawImage(bkImg, 0, 0, 1440, 900, 0, 0, 2048, 1024, null);
	}
	
	private void changeFish() {
		// TODO Auto-generated method stub
		if((index++%10)==0) {
			fish.move();
			for(int i=0;i<fishs.size();i++) {
				fishs.get(i).move();
			}
		}
	}
	
	int index=1;
	public void action() {
		Timer timer=new Timer();
		timer.schedule(new TimerTask() {
		public void run() {

			if(state==1) {
			changeFish();
			createFish();
			hitFish();
			outOfBounds();
			repaint(); 
		  }
		}
	   },20,10);
	}
	private void hitFish() {
		for(int i=0;i<fishs.size();i++) {
				if(hit(fishs.get(i))) {
					if(fish.zhanli>=fishs.get(i).zhanli) {
						
					switch(fishs.get(i).zhanli) {
					case 1:score++;break;
					case 2:score+=5;break;
					case 3:score+=10;break;
					case 4:score+=100;break;
					  }
					fishs.remove(i);
					music.eat.play();
					if(score>=3) fish.zhanli=2;	
					if(score>=20) fish.zhanli=3;
					if(score>=30) fish.zhanli=4;
				}else {
					music.death.play();
					state=3;
					music.bgm.stop();
					}
					if(fish.zhanli>oldzhanli) {
						fish.x1=(int) (fish.x1*1.1);
						fish.y1=(int) (fish.y1*1.1);
						music.levelup.play();
						oldzhanli=fish.zhanli;
					}
			}
	   }
	}
	private void outOfBounds() {
		for(int i=0;i<fishs.size();i++) {
			if(fishs.get(i).isMoveLeft) {
				if(fishs.get(i).x<=0) {
					fishs.remove(i);
				}
			} else
				if(fishs.get(i).x>=1440) {
					fishs.remove(i);
			}
		}	
	}
	
	 public boolean hit(Fish fishs) {
		 int x=fish.x+(fish.x1-fish.x)/2,y=fish.y+(fish.y1-fish.y)/2;
			  if((x>=fishs.x)&&(x<=fishs.x+fishs.length) ){
				  if((y>=fishs.y)&&(y<=fishs.y+fishs.height)) {
					  return true;
				  }
		  }
		return false;
	  }
	
	public void move(int vk) {
		  switch(vk) {
		  case KeyEvent.VK_RIGHT:
			  fish.moveRight();
			   break;
		  case KeyEvent.VK_LEFT:
			  fish.moveLeft();
			   break;
		  case KeyEvent.VK_UP:
			  fish.fishMoveUp();
			   break;
		  case KeyEvent.VK_DOWN:
			  fish.fishMoveDown();
			  break;
		  case KeyEvent.VK_SPACE:if(state==0) {state=RUNNING; music.bgm.loop();}
		  else if(state==3) {state=RUNNING;fish=new myFish();fishs = new ArrayList<Fish>();music.bgm.loop();} break;
		  case KeyEvent.VK_C:;break;
		  case KeyEvent.VK_P:;break;
		  case KeyEvent.VK_S: break;
		  case KeyEvent.VK_Q: ;break;
		  }
	  }
	
}
