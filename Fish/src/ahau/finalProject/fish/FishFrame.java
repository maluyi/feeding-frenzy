package ahau.finalProject.fish;


import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class FishFrame extends JFrame  implements KeyListener{
	//属性
	FishPanel fishPanel;
	Music music = new Music();
	public static final int Width=1440,Heigth=900;
	//方法（构造方法，重写父类的方法，自定义方法）
	     
	 public FishFrame() {
		  setTitle("大鱼吃小鱼");//设置标题
		  setBounds(100,20,Width,Heigth);//设置大小
		  
		  //创建面板对象
		  fishPanel  = new FishPanel();
		  add(fishPanel);
		  fishPanel.action();  
		  setVisible(true);//设置可见
		  //设置窗体关闭后台随之关闭
		  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  addKeyListener(this);
	  }
	 
	  public static void main(String[] args) {
		  new FishFrame(); 
	  }

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		System.out.println(000);
		// TODO Auto-generated method stub
		 int vk = e.getKeyCode();
		  //System.out.println(vk);
		 fishPanel.move(vk);
		 
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	} 
}
