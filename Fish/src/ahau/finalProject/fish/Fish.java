package ahau.finalProject.fish;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

public abstract class Fish {
	int length,height;
    int x,y;
    int sx1,sy1;
    int sx2,sy2;
    int step;
    int count;
    int zhanli;
    Image img;
    boolean isMoveLeft=true;
    Image[] imgs = new Image[10];
    
    public abstract void move() ;
    public void draw(Graphics g) {};
}

class myFish extends Fish{
	int x1,y1;
	public myFish() {
		length= 68;
		height=	55;	
		zhanli=1;
		//界面坐标
		x=100;
		y=450;
		x1=x+68;
		y1=y+55;
		step=0;
		img = new ImageIcon("images/fish08.png").getImage();
	}
	
	public void draw(Graphics g){
		if(isMoveLeft) {
		sy1=height*step;
		sx1=0;
		sy2=height*step+height;
		sx2=length;
		
		}else {
			sy2=height*step;
			sx2=0;
			sy1=height*step+height;

			sx1=length;
			
		}
		g.drawImage(img, x, y, x1, y1, sx1, sy1, sx2,sy2, null);
		
	}
	public void move() {
		if(step<1) {
			step++;
		}else
			step=0;
	}

	public void moveRight() {
		isMoveLeft=false;
		if(x<=1350) {
		x+=10;
		x1+=10;}
	}
	public void moveLeft() {
		isMoveLeft=true;
		if(x>=10) {
		x-=10;
		x1-=10;
		}
	}
	public void fishMoveUp() {
		if(y>=0) {
		y-=10;
		y1-=10;
		}
	}
	public void fishMoveDown() {
		if(y<=800) {
		  y+=10;
		  y1+=10;
		  }
	}
	public void moveUpAndRight() {
		isMoveLeft=false;
		y-=10;
		x+=10;
	}
	public void moveUpAndLeft() {
		isMoveLeft=true;
		y-=10;
		x-=10;
	}
	public void moveDownAndRight() {
		isMoveLeft=false;
		y+=10;
		x+=10;
	}
	public void moveDownAndLeft() {
		isMoveLeft=true;
		y+=10;
		x-=10;
	}
}
